# Maintainer: Jomar Milan <jomarm@jomarm.com>
# Contributor: Tjahneee <t3e@protonmail.com>
pkgname=tic-80
_revision=2837
pkgver=1.1.$_revision
pkgrel=1
pkgdesc='fantasy computer for making, playing, and sharing tiny games.'
arch=('x86_64')
url='https://tic80.com'
license=('MIT')
depends=('gcc-libs' 'hicolor-icon-theme' 'glibc' 'libglvnd' 'curl')
makedepends=('cmake' 'ruby' 'mesa' 'glu')
_argparsever=0d5f5d0745df14a3f373f7eed85bf524714f4524
_blipbufver=330226d9b55ecbeea644e17b5e0f096a165ca07e
_direntver=c885633e126a3a949ec0497273ec13e2c03e862c
_giflibver=1aa11b06d0025eda77b56aec8254130654d4397b
_janetver=169bd812c9183865b2399a8eaeab6aad238856c5
_libpngver=dbe3e0c43e549a1602286144d94b0666549b18e6
_lpegver=1.0.1
_luaver=75ea9ccbea7c4886f30da147fb67b693b2624c26
_moonscriptver=17179283012b392bff972ad66231d73bfeec6e90
_mrubyver=7fe6f3976eca19a86fe533deb4c60b31cd80a236
_msfgifver=7829c8f439d013deeb54eba94761403e1da2a960
_naettver=10a9624456829e5f2b4c264e14760301125d12eb
_pocketpyver=7312afdad24bb308037088d34b32ed0078fed7d2
_quickjsver=b7c0161f2ad1360f0837abafc6abf0edb82c0fa7
_sdlgpuver=e3d350b325a0e0d0b3007f69ede62313df46c6ef
_sdlver=2.26.2
_sokolver=487822d82ca79dba7b67718d962e1ba6beef01b2
_squirrelver=9dcf74f99097898dd5a111c4a55b89d1c4d606c0
_wasm3ver=8523770b80ea1f78afcac21d837f9dfd50f0bed6
_wrenver=4a18fc489f9ea3d253b20dd40f4cdad0d6bb40eb
_zipver=d7df626f3aa457e01669f65e61bf8f484e352941
_zlibver=cacf7f1d4e3d44d871b605da3b647f07d718623f
source=("$pkgname-$pkgver.tar.gz::https://github.com/nesbox/TIC-80/archive/refs/tags/v$pkgver.tar.gz"
        "argparse-$_argparsever.tar.gz::https://github.com/cofyc/argparse/archive/$_argparsever.tar.gz"
        "blip-buf-$_blipbufver.tar.gz::https://github.com/nesbox/blip-buf/archive/$_blipbufver.tar.gz"
        "dirent-$_direntver.tar.gz::https://github.com/tronkko/dirent/archive/$_direntver.tar.gz"
        "giflib-$_giflibver.tar.gz::https://github.com/nesbox/giflib/archive/$_giflibver.tar.gz"
        "janet-$_janetver.tar.gz::https://github.com/janet-lang/janet/archive/$_janetver.tar.gz"
        "libpng-$_libpngver.tar.gz::https://github.com/pnggroup/libpng/archive/$_libpngver.tar.gz"
        "lpeg-$_lpegver.tar.gz::https://www.inf.puc-rio.br/~roberto/lpeg/lpeg-$_lpegver.tar.gz"
        "lua-$_luaver.tar.gz::https://github.com/lua/lua/archive/$_luaver.tar.gz"
        "moonscript-$_moonscriptver.tar.gz::https://github.com/nesbox/moonscript/archive/$_moonscriptver.tar.gz"
        "mruby-$_mrubyver.tar.gz::https://github.com/mruby/mruby/archive/$_mrubyver.tar.gz"
        "msf_gif-$_msfgifver.tar.gz::https://github.com/notnullnotvoid/msf_gif/archive/$_msfgifver.tar.gz"
        "naett-$_naettver.tar.gz::https://github.com/erkkah/naett/archive/$_naettver.tar.gz"
        "pocketpy-$_pocketpyver.tar.gz::https://github.com/pocketpy/pocketpy/archive/$_pocketpyver.tar.gz"
        "quickjs-$_quickjsver.tar.gz::https://github.com/nesbox/quickjs/archive/$_quickjsver.tar.gz"
        "sdl-gpu-$_sdlgpuver.tar.gz::https://github.com/grimfang4/sdl-gpu/archive/$_sdlgpuver.tar.gz"
        "https://github.com/libsdl-org/SDL/releases/download/release-$_sdlver/SDL2-$_sdlver.tar.gz"
        "sokol-$_sokolver.tar.gz::https://github.com/floooh/sokol/archive/$_sokolver.tar.gz"
        "squirrel-$_squirrelver.tar.gz::https://github.com/albertodemichelis/squirrel/archive/$_squirrelver.tar.gz" "wasm3-$_wasm3ver.tar.gz::https://github.com/wasm3/wasm3/archive/$_wasm3ver.tar.gz"
        "wren-$_wrenver::https://github.com/wren-lang/wren/archive/$_wrenver.tar.gz"
        "zip-$_zipver.tar.gz::https://github.com/kuba--/zip/archive/$_zipver.tar.gz"
        "zlib-$_zlibver.tar.gz::https://github.com/madler/zlib/archive/$_zlibver.tar.gz"
        'static-revision.patch')
sha256sums=('a004bbc7b316a4b60d461ac4612fca4ab07e96ed1d1445086e70d1fc4d95961b'
            'b14cfe4b95a4cd1cf148eeecf0dba94ebf5b4312de12f5f5254e7040017477f1'
            'e86870ecb4b7c3feac6afbf5ddd0aff809e14b2d16d9bde251b712fd4ecd6d99'
            'aab1f1d02c3148a3d49a18b8de878603263e62e4a48622121efec92e5b40710b'
            '0061d8b362e1edce6d47c2cf6df218c3d7d4494bacae98d2f0552b8bb1ebc319'
            '57b9254a10cfe9d7dfe8be7d1f5cca06de9b689d1730c93f23f325c9610779a6'
            '43f4a73ee1c25f6c51502629393c91577f47715bf113b8252d6dca859ee462aa'
            '62d9f7a9ea3c1f215c77e0cadd8534c6ad9af0fb711c3f89188a8891c72f026b'
            '9f2f625653b8a6ce9e4489385370256831e71fe2fc15801d9b738aadde45e98d'
            '10f0ebf82ae92a1389d353b349bef25deea594aebb3a65d09f36cd1193d7de18'
            '37d41afb4bee91db3fe34da68a543dcc90a0a5b8c461b331007ef4a0aeeef6d3'
            'd73fe9395b7826e38b734feb90307a50bb32622049e1ec4d6b89ae693dae9aa6'
            'f476b55a6983fc08150ed37cc91dbc0a0a24e05c0891de7e1c86b0536ac8eb76'
            '093b5d02c2b4b21d11edeb49b40c4e91bb49274f91e990b8fdb2dde72f51f6f2'
            'ea8f55b846b2968f02c4a9dfbfca8f30af50ce5a9d3d90a136dce4742b907fdc'
            'e540e0ef477986494ad94b9cc1653dd1d6446221f1850f7299c0a496ddf5945b'
            '95d39bc3de037fbdfa722623737340648de4f180a601b0afad27645d150b99e0'
            'fe8d4f354863c169f18b2b52810d4e7658229226f0f31a5a42ee81bb57a834fe'
            '3516851095b32b812c2821ce28357bf5a533efebb6baee8276d106d4974088a3'
            '0dc7fad64c1d9d65dfa822cf79e471edab36190d8eb0fb7bf87f9064f846299c'
            'fb885f12767ea8b1101145b9d86735995e28c5298b712c1c3f71ed9bbf17f567'
            'f121ff9a6df3414afe958c341c0e3f05d638e43e3be735a9dad8d05ce9322c49'
            '6d4d6640ca3121620995ee255945161821218752b551a1a180f4215f7d124d45'
            '6b90215c282d904a0722c10134d44a4217f024472b15c2f014ccca2f8080e50a')

prepare() {
	# The pkgname is not used because it is in lowercase 'tic-80' and the tarball contains an\
	# uppercase 'TIC-80'
	cd "$srcdir/TIC-80-$pkgver"

	rmdir --ignore-fail-on-non-empty 'vendor/argparse' 'vendor/blip-buf' 'vendor/dirent' \
	'vendor/giflib' 'vendor/janet' 'vendor/libpng' 'vendor/lpeg' 'vendor/lua' 'vendor/moonscript'\
	'vendor/mruby' 'vendor/msf_gif' 'vendor/naett' 'vendor/pocketpy' 'vendor/quickjs'\
	'vendor/sdl-gpu' 'vendor/sdl2' 'vendor/sokol' 'vendor/squirrel' 'vendor/wasm3' 'vendor/wren'\
	'vendor/zip' 'vendor/zlib'

	# Force option helps if this is not a clean build and symlinks were already made
	ln -sf "$srcdir/argparse-$_argparsever" 'vendor/argparse'
	ln -sf "$srcdir/blip-buf-$_blipbufver" 'vendor/blip-buf'
	ln -sf "$srcdir/dirent-$_direntver" 'vendor/dirent'
	ln -sf "$srcdir/giflib-$_giflibver" 'vendor/giflib'
	ln -sf "$srcdir/janet-$_janetver" 'vendor/janet'
	ln -sf "$srcdir/libpng-$_libpngver" 'vendor/libpng'
	ln -sf "$srcdir/lpeg-$_lpegver" 'vendor/lpeg'
	ln -sf "$srcdir/lua-$_luaver" 'vendor/lua'
	ln -sf "$srcdir/moonscript-$_moonscriptver" 'vendor/moonscript'
	ln -sf "$srcdir/mruby-$_mrubyver" 'vendor/mruby'
	ln -sf "$srcdir/msf_gif-$_msfgifver" 'vendor/msf_gif'
	ln -sf "$srcdir/naett-$_naettver" 'vendor/naett'
	ln -sf "$srcdir/pocketpy-$_pocketpyver" 'vendor/pocketpy'
	ln -sf "$srcdir/quickjs-$_quickjsver" 'vendor/quickjs'
	ln -sf "$srcdir/sdl-gpu-$_sdlgpuver" 'vendor/sdl-gpu'
	ln -sf "$srcdir/SDL2-$_sdlver" 'vendor/sdl2'
	ln -sf "$srcdir/sokol-$_sokolver" 'vendor/sokol'
	ln -sf "$srcdir/squirrel-$_squirrelver" 'vendor/squirrel'
	ln -sf "$srcdir/wasm3-$_wasm3ver" 'vendor/wasm3'
	ln -sf "$srcdir/wren-$_wrenver" 'vendor/wren'
	ln -sf "$srcdir/zip-$_zipver" 'vendor/zip'
	ln -sf "$srcdir/zlib-$_zlibver" 'vendor/zlib'

	# Since a tarball is used instead of a git repository, CMake will not be able to use the number
	# of revisions to determine the revision number. An outdated revision number results in an
	# 'outdated version' message.
	patch --strip=1 --input="$srcdir/static-revision.patch"
	sed -i "s/VERSION_REVISION 0/VERSION_REVISION $_revision/" 'CMakeLists.txt'
}

build() {
	cd "$srcdir/TIC-80-$pkgver/build"
	cmake .. -DCMAKE_BUILD_TYPE=MinSizeRel -DBUILD_SDLGPU=On -DBUILD_WITH_ALL=On
	cmake --build . --config MinSizeRel --parallel
}

check() {
	_testpath=$(mktemp -d)

	# I don't think this is a good test, it does not test basic operation of TIC-80 such as
	# saving, loading, and creating carts.
	"$srcdir/TIC-80-$pkgver/build/bin/tic80" --fs "$_testpath" --cli --cmd 'exit'

	rm -r "$_testpath"
}

package() {
	cd "$srcdir/TIC-80-$pkgver/build"

	install -Dm755 'bin/tic80' "$pkgdir/usr/bin/tic80"
	install -Dm755 'bin/player-sdl' "$pkgdir/usr/bin/player-sdl"
	install -Dm755 'bin/bin2txt' "$pkgdir/usr/bin/bin2txt"
	install -Dm755 'bin/cart2prj' "$pkgdir/usr/bin/cart2prj"
	install -Dm755 'bin/prj2cart' "$pkgdir/usr/bin/prj2cart"
	install -Dm755 'bin/wasmp2cart' "$pkgdir/usr/bin/wasmp2cart"
	install -Dm755 'bin/xplode' "$pkgdir/usr/bin/xplode"
	install -Dm644 'linux/tic80.desktop' "$pkgdir/usr/share/applications/tic80.desktop"
	install -Dm644 'linux/tic80.png' "$pkgdir/usr/share/icons/hicolor/256x256/apps/tic80.png"
	install -Dm644 "$srcdir/TIC-80-$pkgver/LICENSE" "$pkgdir/usr/share/licenses/$pkgname/LICENSE"
}
